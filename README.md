# Simple JSON API service example

Basic JSON API service example using REST architecture. Service implements basic CRUD operation for Book and Author models using `gorilla/mux` router.

# Quick start

By default, http-server will start at port `:8080`.

Simple service  start using `Golang`:
```
go run .
```

Run using `Docker` and `docker-compose` via bash script:
``` 
env/manage.sh start
```

CRUD operations example:

| Method | Path    | Operation      | Query example using CLI tool `curl`                                                                                                     |
|--------|---------|----------------|-----------------------------------------------------------------------------------------------------------------------------------------|
| POST   | /book   | Add book       | `curl -v -H "Content-Type: application/json" -d '{"id": 1, "title": "Super book", "author": "Very Author"}' http://localhost:8080/book` |
| GET    | /book   | Get books list | `curl -v -H "Content-Type: application/json" http://localhost:8080/book`                                                                |
| GET    | /book/1 | Get book       | `curl -v -H "Content-Type: application/json" http://localhost:8080/book/1`                                                              |
| PATCH  | /book/1 | Update book    | `curl -v -H "Content-Type: application/json" -d '{"id": 1, "title": "Just a book"}' -X "PATCH" http://localhost:8080/book/1`            |
| DELETE | /book/1 | Delete book    | `curl -v -H "Content-Type: application/json" -X "DELETE" http://localhost:8080/book/1`                                                  |

# Models

__Book__ structure fields and types:
- id - uint
- author - string
- title - string

__Author__ structure fields and types:
- id - uint
- name - string
- description - string

# Docker

To run service inside Docker container you should have Docker and `docker-compose` installed in you operating system.    

Build and start container, run service in it:
``` 
env/manage.sh start
```

Stop container and service:
```
env/manage.sh stop
```

Purge service connected data from Docker:
```
env/manage.sh purge
```
