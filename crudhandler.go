package main

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.com/sergeysynergy/justaservice/models"
	"net/http"
	"strconv"
)

type Response struct {
	Message interface{} `json:"message,omitempty"`
	Error   string      `json:"error,omitempty"`
}

func (r *Response) handleError(w http.ResponseWriter, status int, err error) {
	w.WriteHeader(status)
	r.Error = err.Error()
	CRUDHandlerJson, _ := json.Marshal(r)
	w.Write(CRUDHandlerJson)
}

func (r *Response) handleOk(w http.ResponseWriter, status int, msg interface{}) {
	w.WriteHeader(status)
	r.Message = msg
	CRUDHandlerJson, _ := json.Marshal(r)
	w.Write(CRUDHandlerJson)
}

// ---------------------------------------------------------------------------------------------------------------------

type CRUDHandler struct {
	models.CrudStorer
}

func (c *CRUDHandler) add(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	var resp Response
	model := c.Model()

	err := decoder.Decode(model)
	if err != nil {
		resp.handleError(w, http.StatusBadRequest, err)
		return
	}

	err = c.Add(model)
	if err != nil {
		resp.handleError(w, http.StatusConflict, err)
		return
	}

	msg := fmt.Sprintf("Item %s with ID %d has been successfully added", model.GetName(), model.GetID())
	resp.handleOk(w, http.StatusCreated, msg)
}

func (c *CRUDHandler) get(w http.ResponseWriter, _ *http.Request) {
	var resp Response
	items := c.Get()
	resp.handleOk(w, http.StatusOK, items)
}

func (c *CRUDHandler) find(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	var resp Response

	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		resp.handleError(w, http.StatusBadRequest, err)
		return
	}

	item := c.Find(uint(id))
	if item == nil {
		err := fmt.Errorf("%s with ID %d not found", c.Model().GetName(), id)
		resp.handleError(w, http.StatusNotFound, err)
		return
	}

	resp.handleOk(w, http.StatusOK, item)
}

func (c *CRUDHandler) update(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	var resp Response
	model := c.Model()

	err := decoder.Decode(model)
	if err != nil {
		resp.handleError(w, http.StatusBadRequest, err)
		return
	}

	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		resp.handleError(w, http.StatusBadRequest, err)
		return
	}

	if model.GetID() != uint(id) {
		err = fmt.Errorf("the IDs doesn't match")
		resp.handleError(w, http.StatusBadRequest, err)
		return
	}

	err = c.Update(model)
	if err != nil {
		resp.handleError(w, http.StatusNotFound, err)
		return
	}

	msg := fmt.Sprintf("Item %s with ID %d has been successfully updated", model.GetName(), model.GetID())
	resp.handleOk(w, http.StatusOK, msg)
}

func (c *CRUDHandler) delete(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	var resp Response

	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		resp.handleError(w, http.StatusBadRequest, err)
		return
	}

	err = c.Delete(uint(id))
	if err != nil {
		resp.handleError(w, http.StatusNotFound, err)
		return
	}

	msg := fmt.Sprintf("Item %s with ID %d has been successfully deleted", c.Model().GetName(), id)
	resp.handleOk(w, http.StatusOK, msg)
}
