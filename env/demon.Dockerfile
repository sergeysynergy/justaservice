FROM golang:1.17

### Root section #######################################################################################################
ARG USER
RUN useradd -ms /bin/bash $USER

WORKDIR /workdir
COPY . /workdir
RUN go mod tidy
RUN go install .
RUN chown $USER:$USER -R /workdir

### User section #######################################################################################################
USER $USER
COPY env/demon_bash_aliases /home/$USER/.bash_aliases
ENV TZ="UTC"
CMD ["justaservice"]
