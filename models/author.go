package models

import (
	"fmt"
	"sync"
)

type Author struct {
	ID          uint   `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description,omitempty"`
}

func (a *Author) GetID() uint {
	return a.ID
}

func (a *Author) GetName() string {
	return "author"
}

type AuthorStore struct {
	mu   sync.RWMutex
	byID map[uint]*Author
}

func NewAuthorStore() *AuthorStore {
	return &AuthorStore{
		byID: make(map[uint]*Author, 0),
	}
}

func (a *AuthorStore) Model() CrudModel {
	return new(Author)
}

func (a *AuthorStore) Add(_author interface{}) error {
	a.mu.Lock()
	defer a.mu.Unlock()

	var author *Author
	switch _author.(type) {
	case *Author:
		author = _author.(*Author)
	default:
		return fmt.Errorf("invalide type, need Author")
	}

	_, ok := a.byID[author.ID]
	if ok {
		return fmt.Errorf("author with ID %d already exists", author.ID)
	}

	a.byID[author.ID] = author

	return nil
}

func (a *AuthorStore) Get() interface{} {
	a.mu.RLock()
	defer a.mu.RUnlock()

	return a.byID
}

func (a *AuthorStore) Find(id uint) interface{} {
	a.mu.RLock()
	defer a.mu.RUnlock()

	author, ok := a.byID[id]
	if !ok {
		return nil
	}

	return author
}

func (a *AuthorStore) Update(_newAuthor interface{}) error {
	a.mu.Lock()
	defer a.mu.Unlock()

	var newAuthor *Author
	switch _newAuthor.(type) {
	case *Author:
		newAuthor = _newAuthor.(*Author)
	default:
		return fmt.Errorf("invalide type, need Author")
	}

	author, ok := a.byID[newAuthor.ID]
	if !ok {
		return fmt.Errorf("author with id %d not found", newAuthor.ID)
	}

	// Realization of merging mechanics. Needs if some fields omitted.
	name := newAuthor.Name
	if name == "" {
		name = author.Name
	}
	description := newAuthor.Description
	if description == "" {
		description = author.Description
	}
	*author = Author{
		ID:          author.ID,
		Name:        name,
		Description: description,
	}

	return nil
}

func (a *AuthorStore) Delete(id uint) error {
	a.mu.Lock()
	defer a.mu.Unlock()

	_, ok := a.byID[id]
	if !ok {
		return fmt.Errorf("id %d not found", id)
	}

	delete(a.byID, id)

	return nil
}
