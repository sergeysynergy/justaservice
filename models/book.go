package models

import (
	"fmt"
	"sync"
)

type Book struct {
	ID     uint   `json:"id"`
	Title  string `json:"title"`
	Author string `json:"author"`
}

func (b *Book) GetID() uint {
	return b.ID
}

func (b *Book) GetName() string {
	return "book"
}

type BookStore struct {
	mu   sync.RWMutex
	byID map[uint]*Book
}

func NewBookStore() *BookStore {
	return &BookStore{
		byID: make(map[uint]*Book, 0),
	}
}

func (b *BookStore) Model() CrudModel {
	return new(Book)
}

func (b *BookStore) Add(_book interface{}) error {
	b.mu.Lock()
	defer b.mu.Unlock()

	var book *Book
	switch _book.(type) {
	case *Book:
		book = _book.(*Book)
	default:
		return fmt.Errorf("invalide type, need Book")
	}

	_, ok := b.byID[book.ID]
	if ok {
		return fmt.Errorf("book with ID %d already exists", book.ID)
	}

	b.byID[book.ID] = book

	return nil
}

func (b *BookStore) Get() interface{} {
	b.mu.RLock()
	defer b.mu.RUnlock()

	return b.byID
}

func (b *BookStore) Find(id uint) interface{} {
	b.mu.RLock()
	defer b.mu.RUnlock()

	book, ok := b.byID[id]
	if !ok {
		return nil
	}

	return book
}

func (b *BookStore) Update(_newBook interface{}) error {
	b.mu.Lock()
	defer b.mu.Unlock()

	var newBook *Book
	switch _newBook.(type) {
	case *Book:
		newBook = _newBook.(*Book)
	default:
		return fmt.Errorf("invalide type, need Book")
	}

	book, ok := b.byID[newBook.ID]
	if !ok {
		return fmt.Errorf("book with id %d not found", newBook.ID)
	}

	// Realization of merging mechanics. Needs if some fields omitted.
	title := newBook.Title
	if title == "" {
		title = book.Title
	}
	author := newBook.Author
	if author == "" {
		author = book.Author
	}
	*book = Book{
		ID:     book.ID,
		Title:  title,
		Author: author,
	}

	return nil
}

func (b *BookStore) Delete(id uint) error {
	b.mu.Lock()
	defer b.mu.Unlock()

	_, ok := b.byID[id]
	if !ok {
		return fmt.Errorf("id %d not found", id)
	}

	delete(b.byID, id)

	return nil
}
