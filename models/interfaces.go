package models

type CrudModel interface {
	GetID() uint
	GetName() string
}

type CrudStorer interface {
	Model() CrudModel
	Add(interface{}) error
	Get() interface{}
	Find(uint) interface{}
	Update(interface{}) error
	Delete(uint) error
}
