package main

import (
	"github.com/gorilla/mux"
	"gitlab.com/sergeysynergy/justaservice/models"
	"log"
	"net/http"
	"time"
)

func runMux() {
	r := mux.NewRouter()

	// Add middleware.
	r.Use(
		panicMiddleware,
		accessLogMiddleware,
		addHeaderMiddleware,
	)

	// Create route handlers for Book model.
	bookHandler := CRUDHandler{models.NewBookStore()}
	bookRouter := r.PathPrefix("/book").Subrouter()
	bookRouter.HandleFunc("", bookHandler.add).Methods("POST")
	bookRouter.HandleFunc("", bookHandler.get).Methods("GET")
	bookRouter.HandleFunc("/{id}", bookHandler.find).Methods("GET")
	bookRouter.HandleFunc("/{id}", bookHandler.update).Methods("PATCH")
	bookRouter.HandleFunc("/{id}", bookHandler.delete).Methods("DELETE")

	// Create route handlers for Author model.
	authorHandler := CRUDHandler{models.NewAuthorStore()}
	authorRouter := r.PathPrefix("/author").Subrouter()
	authorRouter.HandleFunc("", authorHandler.add).Methods("POST")
	authorRouter.HandleFunc("", authorHandler.get).Methods("GET")
	authorRouter.HandleFunc("/{id:[0-9]+}", authorHandler.find).Methods("GET")
	authorRouter.HandleFunc("/{id:[0-9]+}", authorHandler.update).Methods("PATCH")
	authorRouter.HandleFunc("/{id:[0-9]+}", authorHandler.delete).Methods("DELETE")

	s := &http.Server{
		Addr:           ":8080",
		Handler:        r,                // if nil use default http.DefaultServeMux
		ReadTimeout:    time.Second * 10, // max duration reading entire request
		WriteTimeout:   time.Second * 10, // max timing write response
		IdleTimeout:    time.Second * 10, // max time wait for the next request
		MaxHeaderBytes: 1 << 20,          // 2^20 = 128 Kb
	}

	go func() {
		log.Printf("starting http server at: %s\n", s.Addr)
		log.Fatal(s.ListenAndServe())
	}()

	graceful(s, time.Second*5)
}
